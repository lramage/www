% www(1)
% Lucas Ramage
% May 22, 2019

# NAME

www - website generator written in c.

# SYNOPSIS

**www** [option] ... [command] ...

# DESCRIPTION

Welcome to the world wide web.

# OPTIONS

**-v, --version**

:   Prints the version number.

# EXIT STATUS

Returns zero on success, errno values on failure.

# NOTES

The www project site, with more information and the source code
repository, can be found at <https://gitlab.com/lramage/www>.

This tool is currently under development, please report any bugs at
the project site or directly to the author.
