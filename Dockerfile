FROM trzeci/emscripten-slim:latest

WORKDIR /usr/src/www
COPY . /usr/src/www

CMD emcc -O2 src/main.c -o index.html