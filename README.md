# www

「 h e l l o , w o r l d 」

_Welcome to the world wide web._

## Dependencies

- [emscripten](https://emscripten.org)
- [libfyaml](https://github.com/pantoniou/libfyaml)
- [pandoc](https://pandoc.org) (optional)

## License

SPDX-License-Identifier: [GPL-3.0-or-later](COPYING)
